﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Handlers
{
    public class GetCategoriesHandler : IRequestHandler<GetCategoriesQuery, IEnumerable<Categories>>
    {
        private readonly IGroceryServices _data;

        public GetCategoriesHandler(IGroceryServices data)
        {
            _data = data;
        }

        public Task<IEnumerable<Categories>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getCategories());
        }
    }
}
