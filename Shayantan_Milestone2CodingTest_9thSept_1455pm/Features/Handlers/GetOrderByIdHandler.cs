﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Handlers
{
    public class GetOrderByIdHandler:IRequestHandler<GetOrderByIdQuery, IEnumerable<ProductOrder>>
    {
        private readonly IGroceryServices _data;

    public GetOrderByIdHandler(IGroceryServices data)
    {
        _data = data;
    }

        public Task<IEnumerable<ProductOrder>> Handle(GetOrderByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getOrder(request.Userid));
        }
    }
}
