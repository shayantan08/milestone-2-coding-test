﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Handlers
{
    public class GetProductsHandler : IRequestHandler<GetProductsQuery, IEnumerable<Product>>
    {
        private readonly IGroceryServices _data;

        public GetProductsHandler(IGroceryServices data)
        {
            _data = data;
        }

        public Task<IEnumerable<Product>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getProducts());
        }
    }
}
