﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Handlers
{
    public class GetProductsByIdHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IGroceryServices _data;

        public GetProductsByIdHandler(IGroceryServices data)
        {
            _data = data;
        }

        public Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getProducts(request.Productid));
        }
    }
}
