﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Commands;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Handlers
{
    public class AddOrderHandler:IRequestHandler<AddOrderCommand,ProductOrder>
    {
        private readonly IGroceryServices _data;

        public AddOrderHandler(IGroceryServices data)
        {
            _data = data;
        }

        public Task<ProductOrder> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.addOrder(request.ProductId, request.UserId));
        }
    }
}
