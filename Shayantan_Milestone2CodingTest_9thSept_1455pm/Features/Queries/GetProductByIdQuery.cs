﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using System.Collections.Generic;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int Productid { get; set; }
    }
}
