﻿using MediatR;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Commands
{
    public class AddOrderCommand : IRequest<ProductOrder>
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
    }
}
