﻿using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence;
using System.Collections.Generic;
using System.Linq;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Repository
{
    public class GroceryServices:IGroceryServices
    {
        private readonly GroceryDBContext _data;

        public GroceryServices(GroceryDBContext data)
        {
            _data = data;
        }

        public ProductOrder addOrder(int pid, int uid)
        {
            var oid = _data.ProductOrder.Max(x => x.Orderid) + 1;
            var prod = _data.Product.SingleOrDefault(x => x.Productid == pid);
            var user = _data.ApplicationUser.SingleOrDefault(x => x.Id == uid);
            var prodorder = new ProductOrder()
            {
                Userid = uid,
                Productid = pid,
                Orderid = oid,
                Product = prod,
                User = user
            };
            _data.ProductOrder.Add(prodorder);
            _data.SaveChanges();
            return prodorder;
        }

        public IEnumerable<Categories> getCategories()
        {
            return _data.Categories.ToList();
        }

        public IEnumerable<ProductOrder> getOrder(int Userid)
        {
            return _data.ProductOrder.Where(o => o.Userid == Userid).ToList();
        }

        public IEnumerable<Product> getProducts()
        {
            return _data.Product.ToList();
        }

        public Product getProducts(int Productid)
        {
            var prod = _data.Product.FirstOrDefault(o => o.Productid == Productid);
            return prod;
        }
    }
}

