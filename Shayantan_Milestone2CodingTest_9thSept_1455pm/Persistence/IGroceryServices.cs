﻿using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using System.Collections.Generic;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Persistence
{
    public interface IGroceryServices
    {
        public IEnumerable<Product> getProducts();
        public IEnumerable<Categories> getCategories();
        public ProductOrder addOrder(int userid, int productid);
        public Product getProducts(int Productid);
        public IEnumerable<ProductOrder> getOrder(int Userid);
    }
}
