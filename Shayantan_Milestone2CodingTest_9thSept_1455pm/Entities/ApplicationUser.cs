﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long? Phone { get; set; }
        [Required]
        public int? Pincode { get; set; }
        [Required]
        public string House { get; set; }
        [Required]
        public string Road { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
