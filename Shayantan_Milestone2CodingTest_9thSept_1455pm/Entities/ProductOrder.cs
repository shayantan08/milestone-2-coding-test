﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities
{
    public partial class ProductOrder
    {
        [Required]
        public int Orderid { get; set; }
        [Required]
        public int Productid { get; set; }
        [Required]
        public int Userid { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
