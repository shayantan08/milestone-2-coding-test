﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        [Required]
        public int Productid { get; set; }
        [Required]
        public string Productname { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal? Amount { get; set; }
        [Required]
        public int? Stock { get; set; }
        [Required]
        public int? Catid { get; set; }
        [Required]
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
