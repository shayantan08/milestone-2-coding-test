﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        [Required]
        public int Catid { get; set; }
        [Required]
        public string Catname { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
