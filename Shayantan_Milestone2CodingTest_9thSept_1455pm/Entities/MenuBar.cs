﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities
{
    public partial class MenuBar
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public bool? Openinnewwindow { get; set; }
    }
}
