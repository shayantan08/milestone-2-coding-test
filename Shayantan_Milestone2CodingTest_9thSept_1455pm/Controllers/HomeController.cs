﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Entities;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Commands;
using Shayantan_Milestone2CodingTest_9thSept_1455pm.Features.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Shayantan_Milestone2CodingTest_9thSept_1455pm.Controllers
{
    [Route("api/[controller]/[action]")]
        [ApiController]
        public class HomeController : ControllerBase
        {
            private readonly IMediator _mediatr;

            public HomeController(IMediator mediatr)
            {
                _mediatr = mediatr;
            }




            // GET: api/<HomeController>
            [HttpGet]
            [Route("getProducts")]
            public async Task<IEnumerable<Product>> Get()
            {
                return await _mediatr.Send(new GetProductsQuery());
            }

            [HttpGet]
            [Route("getCategories")]
            public async Task<IEnumerable<Categories>> GetCategories()
            {
                return await _mediatr.Send(new GetCategoriesQuery());
            }

            // GET api/<HomeController>/5
            [HttpGet]
            [Route("getProductById")]
            public async Task<Product> Get(int prodid)
            {
                return await _mediatr.Send(new GetProductByIdQuery() { Productid = prodid });
            }

            [HttpGet]
            [Route("getOrder")]
            public async Task<IEnumerable<ProductOrder>> GetOrder(int userid)
            {
                return await _mediatr.Send(new GetOrderByIdQuery() { Userid = userid });
            }

            // POST api/<HomeController>
            [HttpPost]
            public async Task<ProductOrder> Post(int userid, int productid)
            {
                return await _mediatr.Send(new AddOrderCommand() { ProductId = productid, UserId = userid });
            }


        }
    }
